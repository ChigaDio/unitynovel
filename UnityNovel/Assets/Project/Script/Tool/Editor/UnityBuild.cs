using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// ビルド
/// </summary>
public class UnityBuild
{
    public static void Build()
    {
        // ビルド対象シーンリスト
        string[] sceneList = {
            "./Assets/scene/Sample.unity",
        };


        // 実行
        var errorMessage = BuildPipeline.BuildPlayer(
                GetEnabledScenes(),                          //!< ビルド対象シーンリスト
               "D:/Desk/Git/UnityNovel/unitynovel/Game/UnityNovel.exe",   //!< 出力先
                BuildTarget.StandaloneWindows,      //!< ビルド対象プラットフォーム
                BuildOptions.Development            //!< ビルドオプション
        );


        // 結果出力
        if (errorMessage.summary.result == UnityEditor.Build.Reporting.BuildResult.Failed)
        {
            foreach(var data in errorMessage.steps)
            {
                Debug.Log("[ERROR]" + data.messages);
            }
        }
        else
        {
            Debug.Log("[Success!]");
        }
    }

    static string[] GetEnabledScenes()
    {
        List<string> sceneList = new List<string>();

        // "Scenes In Build"に登録されているシーンリストを取得
        EditorBuildSettingsScene[] scenes = EditorBuildSettings.scenes;
        foreach (EditorBuildSettingsScene scene in scenes)
        {
            // チェックがついているか確認
            if (scene.enabled)
            {
                // リストに加える
                sceneList.Add(scene.path);
            }
        }

        // 配列にして返す
        return sceneList.ToArray();
    }
}
